from budayaKB_model import BudayaItem, BudayaCollection
from flask import Flask, request, render_template
# from wtforms import Form, validators, TextField

app = Flask(__name__,static_url_path="/static")
app.secret_key ="tp4"

#inisialisasi objek budayaData
databasefilename = ""
budayaData = BudayaCollection()

#inisialisasi custom exception oleh user
class wrongExtension(Exception):
	pass

#merender tampilan default(index.html)
@app.route('/')
def index():
	return render_template("index.html")

# Bagian ini adalah implementasi fitur Impor Budaya, yaitu:
# - merender tampilan saat menu Impor Budaya diklik	
# - melakukan pemrosesan terhadap isian form setelah tombol "Import Data" diklik
# - menampilkan notifikasi bahwa data telah berhasil diimport 	
@app.route("/imporBudaya", methods=["GET", "POST"])
def importData():
	try:
		if request.method == "GET":
			return render_template("imporBudaya.html")

		elif request.method == "POST":
			f = request.files["file"]
			if f.filename.rsplit(".",1)[1] != "csv":
				raise wrongExtension
			global databasefilename #Agar mengarahkan database yang dimaksud adalah variabel global
			databasefilename = f.filename
			budayaData.importFromCSV(databasefilename)
			n_data = len(budayaData.koleksi)
			return render_template("imporBudaya.html", result=n_data, fname=f.filename)
	except wrongExtension:
		return render_template("imporBudaya.html",error="bukan-csv")
	except FileNotFoundError:
		#Untuk case file tidak ditemukan
		return render_template("imporBudaya.html",error="file")

# Bagian ini adalah implementasi fitur Tambah Budaya, yaitu:
# - merender tampilan saat menu Tambah Budaya diklik	
# - melakukan pemrosesan terhadap isian form setelah tombol "Tambahkan Data" diklik
# - menampilkan notifikasi bahwa data yang diinginkan telah berhasil ditambahkan 	
@app.route("/tambahBudaya", methods=["GET","POST"])
def tambahBudaya():
	try:
		if request.method == "GET":
			return render_template("tambahBudaya.html")
		elif request.method == "POST":
			#Menerima input data budaya yang ingin ditambahkan dari user
			nama_budaya = request.form["namabudaya"].title()
			tipe_budaya = request.form["tipebudaya"].title()
			provinsi = request.form["provinsi"].title()
			url = request.form["url"]
			result = budayaData.tambah(nama_budaya,tipe_budaya,provinsi,url)
			if result == 0: #Untuk case budaya yang ingin ditambah sudah tersedia di BudayaKB
				return render_template("tambahBudaya.html",result=0,name=nama_budaya)
			elif result == 1: #Jika tidak ada, maka budaya akan ditambahkan ke dalam BudayaKB
				budayaData.tambah(nama_budaya,tipe_budaya,provinsi,url)
				budayaData.exportToCSV(databasefilename)
				return render_template("tambahBudaya.html",result=1,name=nama_budaya)
	except FileNotFoundError:
		#Untuk case jika file belum diimpor
		budayaData.hapus(nama_budaya) #Agar saat error, budaya tidak tetap ditambahkan ke BudayaKB
		return render_template("tambahBudaya.html",error="file")

# Bagian ini adalah implementasi fitur Ubah Budaya, yaitu:
# - merender tampilan saat menu Ubah Budaya diklik	
# - melakukan pemrosesan terhadap isian form setelah tombol "Ubah Data" diklik
# - menampilkan notifikasi bahwa data telah berhasil diubah
@app.route("/ubahBudaya",methods=["GET","POST"])
def ubahBudaya():
	try:
		if request.method == "GET":
			return render_template("ubahBudaya.html")
		elif request.method == "POST":
			#Menerima input data budaya yang ingin diubah dari user
			nama_budaya = request.form["namabudaya"].title()
			tipe_budaya = request.form["tipebudaya"].title()
			provinsi = request.form["provinsi"].title()
			url = request.form["url"]
			budayaData.ubah(nama_budaya,tipe_budaya,provinsi,url)
			budayaData.exportToCSV(databasefilename)
			return render_template("ubahBudaya.html",result=budayaData.ubah(nama_budaya,tipe_budaya,provinsi,url),name=nama_budaya)
	except FileNotFoundError:
		#Untuk case file belum diimpor
		return render_template("ubahBudaya.html",error="file")

# Bagian ini adalah implementasi fitur Hapus Budaya, yaitu:
# - merender tampilan saat menu Hapus Budaya diklik	
# - melakukan pemrosesan terhadap isian form setelah tombol "Hapus Data" diklik
# - menampilkan notifikasi bahwa data telah berhasil dihapus
@app.route("/hapusBudaya",methods=["GET","POST"])
def hapusBudaya():
	try:
		if request.method == "GET":
			return render_template("hapusBudaya.html")
		elif request.method == "POST":
			#Menerima input data budaya yang ingin dihapus dari user
			nama_budaya = request.form["namabudaya"].title()
			result = budayaData.hapus(nama_budaya)
			if result == 0: #Untuk case jika data yang input tidak tersedia di BudayaKB
				return render_template("hapusBudaya.html",result=0,name=nama_budaya)
			elif result == 1: #Jika ternyata data tersedia, maka data akan dihapus
				budayaData.hapus(nama_budaya)
				budayaData.exportToCSV(databasefilename)
				return render_template("hapusBudaya.html",result=1,name=nama_budaya)
	except FileNotFoundError:
		#Untuk case jika file belum diimpor
		return render_template("hapusBudaya.html",error="file")

# Bagian ini adalah implementasi fitur Cari Budaya, yaitu:
# - merender tampilan saat menu Cari Budaya diklik
# - memberikan pilihan kategori pencarian kepada user	
# - melakukan pemrosesan terhadap isian form setelah tombol "Cari" diklik
# - menampilkan notifikasi bahwa data telah berhasil ditemukan
@app.route("/cariBudaya",methods=["GET","POST"])
def cariBudaya():
	if request.method == "GET":
		return render_template("cariBudaya.html")
	elif request.method == "POST":
		#Menerima input kategori dan data yang ingin dicari oleh user
		kriteria = request.form.get("selection")
		budaya = request.form["input"]
		if kriteria == "nama": #Jika kategori 'nama', maka program akan mencari berdasarkan nama budaya
			hasil_pencarian = budayaData.cariByNama(budaya)
			hasil_pencarian.sort()
			return render_template("cariBudaya.html",result=hasil_pencarian,name=budaya)
		elif kriteria == "tipe": #Jika kategori 'tipe', maka program akan mencari berdasarkan tipe budaya
			hasil_pencarian = budayaData.cariByTipe(budaya)
			hasil_pencarian.sort()
			return render_template("cariBudaya.html",result=hasil_pencarian,name=budaya)
		elif kriteria == "prov": #Jika kategori 'provinsi' maka program akan mencari berdasarkan provinsi
			hasil_pencarian = budayaData.cariByProv(budaya)
			hasil_pencarian.sort()
			return render_template("cariBudaya.html",result=hasil_pencarian,name=budaya)

# Bagian ini adalah implementasi fitur Stat Budaya, yaitu:
# - merender tampilan saat menu Ubah Budaya diklik
# - memberikan pilihan kategori dalam menampilkan stat kepada user
# - melakukan pemrosesan terhadap kategori yang dipilih setelah tombol "Tampilkan Stat" diklik
# - menampilkan notifikasi bahwa stat data telah berhasil ditemukan
@app.route("/statsBudaya",methods=["GET","POST"])
def statsBudaya():
	if request.method == "GET":
		return render_template("statBudaya.html")
	elif request.method == "POST":
		#Menerima input kategori yang diinginkan dari user
		kategori = request.form.get("selection")
		if kategori == "stat":
			#Jika kategorinya 'stat', maka program akan menampilkan jumlah keseluruhan data budaya
			hasil_stat = budayaData.stat()
			return render_template("statBudaya.html",result=False,data=hasil_stat)
		elif kategori == "stattipe":
			#Jika kategorinya 'stattipe', maka program akan menampilkan jumlah stat berdasarkan tipe dan tabel datanya
			hasil = budayaData.statByTipe()
			hasil_stat = hasil.items()
			return render_template("statBudaya.html",result=True,data=hasil_stat,type="Tipe")
		elif kategori == "statprov":
			#Jika kategorinya 'statprov', maka program akan menampilkan jumlah stat berdasarkan provinsi dan tabel datanya
			hasil = budayaData.statByProv()
			hasil_stat = hasil.items()
			return render_template("statBudaya.html",result=True,data=hasil_stat,type="Provinsi")

#Berguna untuk case jika terjadi error 404
@app.errorhandler(404)
def page_not_found(e):
	return render_template("404.html")

# run main app
if __name__ == "__main__":
	app.run(debug=True)
	#debug = True berfungsi agar saat perubahan dilakukan, maka tampilan di browser akan merefresh secara otomatis



